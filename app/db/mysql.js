const mysql = require("mysql");
const CREDENTIALS = require("../config/mysql");

function connection() {
  return mysql.createConnection(CREDENTIALS);
}

function consultCities(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let select = `SELECT * FROM ${process.env.TABLE_CITY}`;
    let query = mysqlConnection.format(select);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function consultRoles(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let select = `SELECT * FROM ${process.env.TABLE_ROLE}`;
    let query = mysqlConnection.format(select);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

/*CRUD user */
function registerUser(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_USER} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function consultUserEmail(email) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let select = `SELECT * FROM ${process.env.TABLE_USER} WHERE email = ?`;
    let query = mysqlConnection.format(select, email);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function consultUser(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let id = data.id;
    let select = `SELECT * FROM ${process.env.TABLE_USER} WHERE id = ?`;
    let query = mysqlConnection.format(select, id);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function consultEmployeeAll(data){
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let select = `SELECT em.*, l.name as localName,  c.name as cityName, r.name as roleName FROM ${process.env.TABLE_USER} em 
    INNER JOIN ${process.env.TABLE_LOCAL} l ON em.localCode = l.id 
    INNER JOIN ${process.env.TABLE_ROLE} r ON em.roleCode = r.id 
    INNER JOIN ${process.env.TABLE_CITY} c ON em.cityCode = c.id`;
    
    let query = mysqlConnection.format(select);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function updateUser(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let id = data.id;
    let update = `UPDATE ${process.env.TABLE_USER} SET ? WHERE id = ?`;
    let query = mysqlConnection.format(update, [data.req.body, id]);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function deleteUser(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    
    let id = data.id;
    let deleteUser = `DELETE FROM ${process.env.TABLE_USER} WHERE id = ?`;
    let query = mysqlConnection.format(deleteUser, id);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}
function login(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let email = data.email;
    let select = `SELECT id, email, password, name, roleCode FROM ${process.env.TABLE_USER} WHERE email = ?`;
    let query = mysqlConnection.format(select, email);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

/*CRUD local */
function registerLocal(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_LOCAL} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function consultLocal(){
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let select = `SELECT lo.*, c.name as cityName FROM ${process.env.TABLE_LOCAL} lo INNER JOIN ${process.env.TABLE_CITY} c ON lo.cityCode = c.id`;
    let query = mysqlConnection.format(select);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function updateLocal(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let id = data.query.id;
    let update = `UPDATE ${process.env.TABLE_LOCAL} SET ? WHERE id = ?`;
    let query = mysqlConnection.format(update, [data.body, id]);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function deleteLocal(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let id = data.query.id;
    let deleteLocal = `DELETE FROM ${process.env.TABLE_LOCAL} WHERE id = ?`;
    let query = mysqlConnection.format(deleteLocal, id);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

/*CRUD customer */
function registerCustomer(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_CUSTOMER} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function consultCustomer(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let document = data.document;
    let select = `SELECT cu.*, c.name as cityName FROM ${process.env.TABLE_CUSTOMER} cu INNER JOIN ${process.env.TABLE_CITY} c ON cu.cityCode = c.id`;
    let query = mysqlConnection.format(select);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function updateCustomer(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let document = data.query.document;
    let update = `UPDATE ${process.env.TABLE_CUSTOMER} SET ? WHERE document= ?`;
    let query = mysqlConnection.format(update, [data.body, document]);
    console.log(query);
    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function deleteCustomer(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let document = data.document;
    let deleteCustomer = `DELETE FROM ${process.env.TABLE_CUSTOMER} WHERE document = ?`;
    let query = mysqlConnection.format(deleteCustomer, [document]);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve("Customer deleted");
    });
  });
}

/*Condult cities*/


/*CRUD Product */
function registerProduct(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_PRODUCT} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function consultProduct(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let code = data.code;
    let select = `SELECT * FROM ${process.env.TABLE_PRODUCT}`;
    let query = mysqlConnection.format(select, [code]);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function updateProduct(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let code = data.query.code;
    let update = `UPDATE ${process.env.TABLE_PRODUCT} SET ? WHERE code= ?`;
    let query = mysqlConnection.format(update, [data.body, code]);
    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function deleteProduct(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let code = data.code;
    let deleteProduct = `DELETE FROM ${process.env.TABLE_PRODUCT} WHERE code = ?`;
    let query = mysqlConnection.format(deleteProduct, [code]);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve("product deleted");
    });
  });
}

/*CRUD sale */
function registerSale(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_SALE} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function consultSale() {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let select = `SELECT s.id, date_format(s.date, "%d/%m/%Y") AS date, s.iva, s.methodPayment, s.total, s.shipping, s.localCode, l.name AS localName, s.customerDocument FROM ${process.env.TABLE_SALE} s INNER JOIN ${process.env.TABLE_LOCAL} l ON s.localCode = l.id`;
    let query = mysqlConnection.format(select);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function updateSale(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let id = data.query.id;
    let update = `UPDATE ${process.env.TABLE_SALE} SET ? WHERE id = ?`;
    let query = mysqlConnection.format(update, [data.body, id]);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function deleteSale(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let id = data.query.id;
    let deleteSale = `DELETE FROM ${process.env.TABLE_SALE} WHERE id = ?`;
    let query = mysqlConnection.format(deleteSale, id);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function registerSaleDetail(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_SALEDETAIL} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (err, result) => {
      if (err) reject(err);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

module.exports = {
  registerUser,
  consultUser,
  consultUserEmail,
  updateUser,
  deleteUser,
  login,
  registerLocal,
  consultLocal,
  updateLocal,
  deleteLocal,
  registerCustomer,
  consultCustomer,
  updateCustomer,
  deleteCustomer,
  consultCities,
  registerSale,
  consultSale,
  updateSale,
  deleteSale,
  registerSaleDetail,
  registerProduct,
  consultProduct,
  updateProduct,
  deleteProduct,
  consultRoles,
  consultEmployeeAll,
};
