-- Adminer 4.8.1 MySQL 5.7.36 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS hr_metalisteria;
CREATE SCHEMA hr_metalisteria;
USE hr_metalisteria;

/* DROP TABLE IF EXISTS `arl`;
CREATE TABLE `arl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` char(20) NOT NULL,
  `fechaIngeso` date NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */


/* DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` char(20) NOT NULL,
  `horario` int(11) NOT NULL,
  `salario` double NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */


/* DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` char(30) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */

DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  `countryId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `department_country_FK` (`countryId`),
  CONSTRAINT `department_country_FK` FOREIGN KEY (`countryId`) REFERENCES `country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  `departmentId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `city_department_FK` (`departmentId`),
  CONSTRAINT `city_department_FK` FOREIGN KEY (`departmentId`) REFERENCES `department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `document` int(11) NOT NULL,
  `name` char(25) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phoneNumber` char(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `cityCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`document`),
  UNIQUE KEY `cedula_UNIQUE` (`document`),
  KEY `cityCode_idx` (`cityCode`),
  CONSTRAINT `cityCode` FOREIGN KEY (`cityCode`) REFERENCES `city` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* DROP TABLE IF EXISTS `detalleventa`;
CREATE TABLE `detalleventa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Venta_codigo` varchar(25) NOT NULL,
  `ProductoLocal_id` int(11) NOT NULL,
  `cantidad` smallint(6) NOT NULL,
  `precio` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `ProductoVenta_ProductoLocal_FK` (`ProductoLocal_id`),
  KEY `ProductoVenta_Venta_FK` (`Venta_codigo`),
  CONSTRAINT `ProductoVenta_ProductoLocal_FK` FOREIGN KEY (`ProductoLocal_id`) REFERENCES `productolocal` (`id`),
  CONSTRAINT `ProductoVenta_Venta_FK` FOREIGN KEY (`Venta_codigo`) REFERENCES `venta` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */


DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `phoneNumber` char(15) NOT NULL,
  `address` char(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `localCode` int(11) NOT NULL,
  `cityCode` int(11) NOT NULL,
  `roleCode` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `employee_local_FK` (`localCode`),
  KEY `employee_city_FK` (`cityCode`),
  KEY `employee_role_FK` (`roleCode`),
  CONSTRAINT `employee_local_FK` FOREIGN KEY (`localCode`) REFERENCES `local` (`id`),
  CONSTRAINT `employee_city_FK` FOREIGN KEY (`cityCode`) REFERENCES `city` (`id`),
  CONSTRAINT `employee_role_FK` FOREIGN KEY (`roleCode`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* 
DROP TABLE IF EXISTS `empleadocargo`;
CREATE TABLE `empleadocargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Empleado_cedula` int(11) NOT NULL,
  `Cargo_id` int(11) NOT NULL,
  `Maquina_codigo` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `EmpleadoCargo_Maquina_FK` (`Maquina_codigo`),
  KEY `Funcion_Cargo_FK` (`Cargo_id`),
  KEY `Funcion_Empleado_FK` (`Empleado_cedula`),
  CONSTRAINT `EmpleadoCargo_Maquina_FK` FOREIGN KEY (`Maquina_codigo`) REFERENCES `maquina` (`codigo`),
  CONSTRAINT `Funcion_Cargo_FK` FOREIGN KEY (`Cargo_id`) REFERENCES `cargo` (`id`),
  CONSTRAINT `Funcion_Empleado_FK` FOREIGN KEY (`Empleado_cedula`) REFERENCES `empleado` (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */


/* DROP TABLE IF EXISTS `eps`;
CREATE TABLE `eps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` char(30) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */

/* 
DROP TABLE IF EXISTS `fabrica`;
CREATE TABLE `fabrica` (
  `codigo` varchar(25) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `Ciudad_id` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`),
  KEY `Fabrica_Ciudad_FK` (`Ciudad_id`),
  CONSTRAINT `Fabrica_Ciudad_FK` FOREIGN KEY (`Ciudad_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */

/* 
DROP TABLE IF EXISTS `fondopensiones`;
CREATE TABLE `fondopensiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` char(50) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */

DROP TABLE IF EXISTS `local`;
CREATE TABLE `local` (
  `id`  int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `cityCode` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `local_city_FK` (`cityCode`),
  CONSTRAINT `local_city_FK` FOREIGN KEY (`cityCode`) REFERENCES `city` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* 
DROP TABLE IF EXISTS `maquina`;
CREATE TABLE `maquina` (
  `codigo` varchar(25) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `marca` char(50) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `Fabrica_codigo` varchar(25) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `Maquina_Fabrica_FK` (`Fabrica_codigo`),
  CONSTRAINT `Maquina_Fabrica_FK` FOREIGN KEY (`Fabrica_codigo`) REFERENCES `fabrica` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */

/* DROP TABLE IF EXISTS `pedido`;
CREATE TABLE `pedido` (
  `codigo` varchar(25) NOT NULL,
  `precioTotal` double NOT NULL,
  `cantidadUnidades` smallint(6) NOT NULL,
  `fechaSolicitud` date NOT NULL,
  `descuento` double DEFAULT NULL,
  `Proveedor_id` varchar(25) NOT NULL,
  `Fabrica_codigo` varchar(25) NOT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`),
  KEY `Pedido_Fabrica_FK` (`Fabrica_codigo`),
  KEY `Pedido_Proveedor_FK` (`Proveedor_id`),
  CONSTRAINT `Pedido_Fabrica_FK` FOREIGN KEY (`Fabrica_codigo`) REFERENCES `fabrica` (`codigo`),
  CONSTRAINT `Pedido_Proveedor_FK` FOREIGN KEY (`Proveedor_id`) REFERENCES `proveedor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */

/* DROP TABLE IF EXISTS `pqr`;
CREATE TABLE `pqr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` char(25) NOT NULL,
  `fecha` date NOT NULL,
  `contenido` varchar(200) NOT NULL,
  `Cliente_cedula` int(11) NOT NULL,
  `EmpleadoCargo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `PQR_Cliente_FK` (`Cliente_cedula`),
  KEY `PQR_EmpleadoCargo_FK` (`EmpleadoCargo_id`),
  CONSTRAINT `PQR_EmpleadoCargo_FK` FOREIGN KEY (`EmpleadoCargo_id`) REFERENCES `empleadocargo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */


DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `code` varchar(25) NOT NULL,
  `name` char(30) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `iva` double NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `codigo_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* DROP TABLE IF EXISTS `productolocal`;
CREATE TABLE `productolocal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `precio` double NOT NULL,
  `Producto_codigo` varchar(25) NOT NULL,
  `Local_codigo` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `ProductoLocal_Local_FK` (`Local_codigo`),
  KEY `ProductoLocal_Producto_FK` (`Producto_codigo`),
  CONSTRAINT `ProductoLocal_Local_FK` FOREIGN KEY (`Local_codigo`) REFERENCES `local` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; */


/* DROP TABLE IF EXISTS `proveedor`;
CREATE TABLE `proveedor` (
  `id` varchar(25) NOT NULL,
  `nombre` char(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` char(20) NOT NULL,
  `Ciudad_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `Proveedor_Ciudad_FK` (`Ciudad_id`),
  CONSTRAINT `Proveedor_Ciudad_FK` FOREIGN KEY (`Ciudad_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */

DROP TABLE IF EXISTS `sale`;
CREATE TABLE `sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `iva` double NOT NULL,
  `methodPayment` char(30) NOT NULL,
  `total` double NOT NULL,
  `shipping` double NOT NULL,
  `localCode` int(11) NOT NULL,
  `customerDocument` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `sale_local_FK` (`localCode`),
  CONSTRAINT `sale_local_FK` FOREIGN KEY (`localCode`) REFERENCES `local` (`id`),
  KEY `sale_customer_FK` (`customerDocument`),
  CONSTRAINT `sale_customer_FK` FOREIGN KEY (`customerDocument`) REFERENCES `customer` (`document`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO country VALUES (1, 'Colombia');

INSERT INTO department VALUES (1, 'Quindio', 1);

INSERT INTO city VALUES (1, 'Armenia', 1);
INSERT INTO city VALUES (2, 'Montenegro', 1);
INSERT INTO city VALUES (3, 'Calarcá', 1);
INSERT INTO city VALUES (4, 'Circasia', 1);
INSERT INTO city VALUES (5, 'La Tebaida', 1);
INSERT INTO city VALUES (6, 'Salento', 1);
INSERT INTO city VALUES (7, 'Quimbaya', 1);
INSERT INTO city VALUES (8, 'Filandia', 1);

INSERT INTO local VALUES (1, 'Local Armenia Centro', 'Centro', 1);
INSERT INTO local VALUES (2, 'Local Armenia Norte', 'Norte', 1);
INSERT INTO local VALUES (3, 'Local Armenia Sur', 'Sur', 1);

INSERT INTO role VALUES (1, 'Administrador');
INSERT INTO role VALUES (2, 'Contador');
INSERT INTO role VALUES (3, 'Inventario');
INSERT INTO role VALUES (4, 'Vendedor');
INSERT INTO role VALUES (5, 'Recursos Humanos');	

-- 2022-04-15 20:07:08