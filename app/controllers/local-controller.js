const db = require("../db/mysql");

let registerLocal = (req, res) => {
  db.registerLocal(req.body)
    .then((result) => {
      return res.status(200).json({
        status: "Added local successfully",
        reg: true,
        locals: result,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let consultLocal = (req, res) => {
  db.consultLocal()
    .then((result) => {
      return res.status(200).json({
        status: "Consulted local successfully",
        locals: result,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let updateLocal = (req, res) => {
  db.updateLocal(req)
    .then((result) => {
      if (result.changedRows == 1) {
        return res.status(200).json({
          status: "Updated local successfully",
          update: true,
          locals: result,
        });
      } else {
        return res.status(400).json({
          status: "No changes were made",
          updae: false,
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

let deleteLocal = (req, res) => {
  db.deleteLocal(req)
    .then((result) => {
      if (result.affectedRows == 1) {
        return res.status(200).json({
          status: "Deleted local successfully",
          delete: true,
        });
      } else {
        return res.status(400).json({
          status: "No changes were made",
          delete: false,
        });
      }
    })
    .catch((err) => {
      return res.status(400).json({
        status: "400",
        message: err,
      });
    });
};

module.exports = {
  registerLocal,
  consultLocal,
  updateLocal,
  deleteLocal,
};