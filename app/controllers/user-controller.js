const db = require("../db/mysql");
const bcrypt = require("bcryptjs");
const nJwt = require("njwt");
const config = require("../config/keys");

let registerUser = (req, res) => {
  let hashPass = bcrypt.hashSync(req.body.password, 8);
  req.body.password = hashPass;
  req.body.roleCode = req.body.roleCode || 1 ;
  db.registerUser(req.body)
    .then((result) => {
      return res.status(200).json({
        status: "Successful registration",
        reg: true,
        user: result,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let consultUser = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, (err, verifiedJwt) => {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.consultUser({ id: verifiedJwt.body.id })
      .then((result) => {
        return res.status(200).json({
          status: "Successful authentication",
          auth: true,
          user: result,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let consultEmployeeAll = (req, res) => {
  db.consultEmployeeAll(req.query)
    .then((result) => {
      return res.status(200).json({
        status: "Ok",
        consult: true,
        users: result,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let updateUser = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, (err, verifiedJwt) => {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.updateUser({ req, id: verifiedJwt.body.id })
      .then((result) => {
        if (result.changedRows == 1) {
          return res.status(200).json({
            status: "Successful update",
          });
        }
        return res.status(400).json({
          status: "Error updating user",
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let adminUpdateUser = (req, res) => {
   db.updateUser({ req, id: req.query.id })
     .then((result) => {
       if (result.changedRows === 1) {
         return res.status(200).json({
           status: "Updated employee successfully",
           auth: true,
         });
       }
       return res.status(400).json({
         status: "400",
         data: "Employee could not be updated",
       });
     })
     .catch((error) => {
       console.log(error);
     });
};

let deleteUser = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, (err, verifiedJwt) => {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.deleteUser({ id: verifiedJwt.body.id })
      .then((result) => {
        return res.status(200).json({
          status: "Successful delete",
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let adminDeleteUser = (req, res) => {
  db.deleteUser(req.query)
    .then((result) => {
      return res.status(200).json({
        status: "Deleted employee successfully",
        auth: true,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};


module.exports = {
  registerUser,
  consultUser,
  updateUser,
  deleteUser,
  consultEmployeeAll,
  adminUpdateUser,
  adminDeleteUser,
};