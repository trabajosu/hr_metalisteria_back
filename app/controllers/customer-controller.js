const db = require("../db/mysql");

let registerCustomer = (req, res) => {
  db.registerCustomer(req.body)
    .then((result) => {
      return res.status(200).json({
        status: "added customer successfully",
        add: true,
        customer: result,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

let consultCustomer = (req, res) => {
  db.consultCustomer(req.query)
    .then((result) => {
      return res.status(200).json({
        status: "Ok",
        consult: true,
        customer: result,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let updateCustomer = (req, res) => {
  db.updateCustomer(req)
    .then((result) => {
      if (result.changedRows === 1) {
        return res.status(200).json({
          status: "updated customer successfully",
          auth: true,
        });
      }
      return res.status(400).json({
        status: "400",
        data: "Customer could not be updated",
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

let deleteCustomer = (req, res) => {
  db.deleteCustomer(req.query)
    .then((result) => {
      return res.status(200).json({
        status: "deleted customer successfully",
        auth: true,
      });
    })
    .catch((error) => {
      console.log(error);;
    });
};

module.exports = {
  registerCustomer,
  updateCustomer,
  consultCustomer,
  deleteCustomer,
};
