const db = require("../db/mysql");

let registerProduct = (req, res) => {
    db.registerProduct(req.body)
        .then((result) => {
            return res.status(200).json({
                status: "added product successfully",
                add: true,
            });
        })
        .catch(function (error) {
            console.log(error);
        });
};

let consultProduct = (req, res) => {
    db.consultProduct(req.query)
      .then((result) => {
        return res.status(200).json({
          status: "Ok",
          consult: true,
          product: result,
        });
      })
      .catch((err) => {
        console.log(err);
      });
};

let updateProduct = (req, res) => {
    db.updateProduct(req)
      .then((result) => {
        if (result.changedRows === 1) {
          return res.status(200).json({
            status: "updated product successfully",
            auth: true,
          });
        }
        return res.status(400).json({
          status: "400",
          data: "Product could not be updated",
        });
      })
      .catch((error) => {
        console.log(error);
      });
};
  
let deleteProduct = (req, res) => {
  db.deleteProduct(req.query)
    .then((result) => {
      return res.status(200).json({
        status: "deleted product successfully",
        auth: true,
      });
    })
    .catch((error) => {
      logger.error(error);
    });
};

module.exports = {
    registerProduct,
    consultProduct,
    updateProduct,
    deleteProduct,
};