const db = require("../db/mysql");

let consultRoles = (req, res) => {
  db.consultRoles()
    .then((result) => {
      return res.status(200).json({
        status: "Ok",
        roles: result,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = {
  consultRoles,
};
