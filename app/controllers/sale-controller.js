const db = require("../db/mysql");

let registerSale = (req, res) => {
  let currentDate = new Date();
  let currentYear = currentDate.getFullYear();
  let currentMonth = currentDate.getMonth() + 1;
  let currentDay = currentDate.getDate();
  req.body["date"] = `${currentYear}-${currentMonth}-${currentDay}`;
  db.registerSale(req.body)
    .then((result) => {
    /*   req.saleDetails.forEach( async saleDetail => {
        saleDetail["saleId"] = result[0].id;
       await db.registerSaleDetail(saleDetail);
      }); */
      return res.status(200).json({
        status: "Registered sale successfully",
        reg: true,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let consultSale = (req, res) => {
  db.consultSale()
    .then((result) => {
      return res.status(200).json({
        status: "Consulted sales successfully",
        sales: result,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let updateSale = (req, res) => {
  db.updateSale(req)
    .then((result) => {
      if (result.changedRows == 1) {
        return res.status(200).json({
          status: "Updated sale successfully",
          update: true,
        });
      } else {
        return res.status(400).json({
          status: "No changes were made",
          updae: false,
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

let deleteSale = (req, res) => {
  db.deleteSale(req)
    .then((result) => {
      if (result.affectedRows == 1) {
        return res.status(200).json({
          status: "Deleted sale successfully",
          delete: true,
        });
      } else {
        return res.status(400).json({
          status: "No changes were made",
          delete: false,
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = {
  registerSale,
  consultSale,
  updateSale,
  deleteSale,
};