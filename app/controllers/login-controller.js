const db = require("../db/mysql");
const bcrypt = require("bcryptjs");
const nJwt = require("njwt");
const config = require("../config/keys");

let login = (req, res) => {
  let email = req.body.email;
  let password = req.body.password;

  db.login(req.body)
    .then((result) => {
      if (result.length > 0) {
        if (!bcrypt.compareSync(password, result[0].password)) {
          return res.status(401).json({
            status: "Authentication failed",
            auth: false,
            message: "Wrong password",
          });
        }
      } else {
        return res.status(401).json({
          status: "Authentication failed",
          auth: false,
          message: "The user does not exist",
        });
      }

      //TOKEN
      let jwt = nJwt.create(
        {
          id: result[0].id,
          email: result[0].email,
          role: result[0].roleCode,
        },
        config.SIGNING_KEY
      );
      jwt.setExpiration(new Date().getTime() + 60 * 60 * 1000);
      let token = jwt.compact();
      return res.status(200).json({
        status: "Successful authentication",
        auth: true,
        token: token,
        info: [result[0].name, result[0].roleCode]
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = {
  login,
};