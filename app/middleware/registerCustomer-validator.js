const { check, validationResult } = require("express-validator");

validatorParams = [
  check("document").isLength({ min: 3 }),
  check("name").isLength({ min: 3 }),
  check("email").isEmail(),
  check("phoneNumber").isLength({ min: 10 }),
  check("address").isLength({ min: 1 }),
  check("cityCode").isLength({ min: 1}),
];

function validator(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  next();
}

module.exports = {
    validatorParams,
    validator 
};