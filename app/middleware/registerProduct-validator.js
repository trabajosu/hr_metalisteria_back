const { check, validationResult } = require("express-validator");

validatorParams = [
  check("code").isLength({ min: 1 }),
  check("name").isLength({ min: 3 }),
  check("description").isLength({ min: 5 }),
  check("iva").isNumeric(),
  check("price").isNumeric(),
];

function validator(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  }
  
  module.exports = {
      validatorParams,
      validator 
  };