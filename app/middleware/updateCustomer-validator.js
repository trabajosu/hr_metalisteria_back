const { check, validationResult } = require("express-validator");

validatorParams = [
    check("name").isLength({ min: 3 }),
    check("email").isEmail(),
    check("phoneNumber").isLength({ min: 10 }),
    check("address").isLength({ min: 3 }),
    check("cityCode").isLength({ min: 1}),
]

function validator(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    next();
}

module.exports = {
    validatorParams,
    validator
};