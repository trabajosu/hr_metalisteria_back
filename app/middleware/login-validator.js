const { check } = require("express-validator");
const validateFields = require("./validate-fields");

validatorLogin = [
  check("email").isEmail(),
  check("password").isLength({ min: 5, max: 15 }),
  validateFields
];

module.exports = {
  validatorLogin
};