const { check } = require("express-validator");
const validateFields = require("./validate-fields");

validatorRegisterLocal = [
  check("name").isLength({ min: 1 }),
  check("address").isLength({ min: 1 }),
  check("cityCode").isNumeric(),
  validateFields,
];

module.exports = {
  validatorRegisterLocal
};