const { check } = require("express-validator");
const validateFields = require("./validate-fields");
const { consultUserEmail } = require("../db/mysql");

validatorRegisterUser = [
  check("name").isLength({ min: 3 }),
  check("email").isEmail(),
  check("email").custom(async (email = "") => {
    const user = await consultUserEmail(email);
    if (user[0]) throw new Error("Email already registered");
  }),
  check("password").isLength({ min: 5, max: 15 }),
  check("address").isLength({ min: 1 }),
  check("phoneNumber").isLength({ min: 10 }),
  check("localCode").isNumeric(),
  check("cityCode").isNumeric(),
  validateFields,
];

validatorUpdateUser = [
  check("name").isLength({ min: 3 }),
  check("address").isLength({ min: 1 }),
  check("phoneNumber").isLength({ min: 10 }),
  check("email").isEmail(),
  check("localCode").isNumeric(),
  check("cityCode").isNumeric(),
  check("roleCode").isNumeric(),
  validateFields,
];

validatorDeleteUser = [
  check("id").isNumeric(),
  validateFields,
];

module.exports = {
  validatorRegisterUser,
  validatorUpdateUser,
  validatorDeleteUser
};