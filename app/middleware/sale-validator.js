const { check } = require("express-validator");
const validateFields = require("./validate-fields");

validatorRegisterSale = [
  check("iva").isNumeric(),
  check("total").isNumeric(),
  check("localCode").isNumeric(),
  check("shipping").isNumeric(),
  check("customerDocument").isNumeric(),
  check("methodPayment").isLength({ min: 1 }),
  validateFields,
];

module.exports = {
  validatorRegisterSale,
};
