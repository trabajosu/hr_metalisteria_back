const nJwt = require("njwt");
const config = require("../config/keys");

let nJwtAuth = (req, res, next) => {
  if (!req.header("Authorization")) {
    return res.status(403).send({
      status: "Authentication failed",
      auth: false,
      message: "No token provided",
    });
  }

  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, (err, verifiedJwt) => {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    req.user = verifiedJwt.body;
    next();
  });
};

module.exports = {
  nJwtAuth,
};