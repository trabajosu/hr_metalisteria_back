require("dotenv").config();
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const bearerToken = require("express-bearer-token");

const login = require("./routes/login");
const user = require("./routes/user");
const local = require("./routes/local");
const customer = require("./routes/customer");
const city = require("./routes/cities");
const role = require("./routes/role");
const sale = require("./routes/sale");
const product = require("./routes/product");

const app = express()
  .use(cors({ credentials: true, origin: "http://localhost:4200" }))
  .use(bodyParser.urlencoded({ extended: false }))
  .use(bodyParser.json())
  .use(bearerToken());

app.use("/login", login);
app.use("/user", user);
app.use("/local", local);
app.use("/customer", customer);
app.use("/city", city);
app.use("/role", role)
app.use("/sale", sale);
app.use("/product", product);

module.exports = app;