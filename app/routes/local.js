const express = require("express");
const authToken = require("../middleware/auth-token");
const { registerLocal, consultLocal, updateLocal, deleteLocal } = require("../controllers/local-controller");
const { validatorRegisterLocal } = require("../middleware/local-validator");
const router = express.Router();

router.post("/register", authToken.nJwtAuth, validatorRegisterLocal, registerLocal);
router.get("/consult", consultLocal);
router.put("/update", authToken.nJwtAuth, validatorRegisterLocal, updateLocal);
router.delete("/delete", authToken.nJwtAuth, deleteLocal);

module.exports = router;