const express = require('express');
const consultCitiesController = require('../controllers/consultCities-controller');
const router = express.Router();

router.get('/consult', consultCitiesController.consultCities);

module.exports = router; 