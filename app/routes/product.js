const express = require("express");
const authToken = require("../middleware/auth-token");
const productController = require("../controllers/product-controller");
const registerProductValidator = require("../middleware/registerProduct-validator");
const updateProductValidator = require("../middleware/updateProduct-validator");
const deleteProductValidator = require("../middleware/deleteProduct-validator");
const router = express.Router();

router.post('/register', authToken.nJwtAuth, registerProductValidator.validatorParams, registerProductValidator.validator, productController.registerProduct);

router.put('/update',  authToken.nJwtAuth, updateProductValidator.validatorParams, updateProductValidator.validator, productController.updateProduct);

router.get("/consult", authToken.nJwtAuth, productController.consultProduct);

router.delete('/delete', authToken.nJwtAuth, deleteProductValidator.validatorParams, deleteProductValidator.validator, productController.deleteProduct);

module.exports = router;
