const express = require("express");
const authToken = require("../middleware/auth-token");
const { registerUser, consultUser, consultEmployeeAll, updateUser, adminUpdateUser, deleteUser, adminDeleteUser} = require("../controllers/user-controller");
const { validatorRegisterUser, validatorUpdateUser, validatorDeleteUser } = require("../middleware/user-validator");
const router = express.Router();

router.post("/register", validatorRegisterUser, registerUser);
router.post("/adminRegister", authToken.nJwtAuth, validatorRegisterUser, registerUser);
router.get("/consult", authToken.nJwtAuth, consultUser);
router.get("/consultAll", authToken.nJwtAuth, consultEmployeeAll);
router.put("/update", authToken.nJwtAuth, validatorUpdateUser, updateUser);
router.put("/adminUpdate", authToken.nJwtAuth, validatorUpdateUser, adminUpdateUser);
router.delete("/delete", authToken.nJwtAuth, deleteUser);
router.delete("/adminDelete", authToken.nJwtAuth, validatorDeleteUser, adminDeleteUser);

module.exports = router;
