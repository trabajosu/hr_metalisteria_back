const express = require("express");
const authToken = require("../middleware/auth-token");
const customerController = require("../controllers/customer-controller");
const registerCustomerValidator = require("../middleware/registerCustomer-validator");
const updateCustomerValidator = require("../middleware/updateCustomer-validator");
const deleteCustomerValidator = require("../middleware/deleteCustomer-validator");
const router = express.Router();

router.post('/register', authToken.nJwtAuth, registerCustomerValidator.validatorParams, registerCustomerValidator.validator, customerController.registerCustomer);

router.put('/update', authToken.nJwtAuth, updateCustomerValidator.validatorParams, updateCustomerValidator.validator, customerController.updateCustomer);

router.get("/consult", authToken.nJwtAuth, customerController.consultCustomer);

router.delete('/delete',  authToken.nJwtAuth, deleteCustomerValidator.validatorParams, deleteCustomerValidator.validator, customerController.deleteCustomer);

module.exports = router;