const express = require("express");
const authToken = require("../middleware/auth-token");
const { registerSale, consultSale, updateSale, deleteSale } = require("../controllers/sale-controller");
const { validatorRegisterSale } = require("../middleware/sale-validator");
const router = express.Router();

router.post("/register", authToken.nJwtAuth, validatorRegisterSale, registerSale);
router.get("/consult", authToken.nJwtAuth, consultSale);
router.put("/update", authToken.nJwtAuth, validatorRegisterSale, updateSale);
router.delete("/delete", authToken.nJwtAuth, deleteSale);

module.exports = router;
