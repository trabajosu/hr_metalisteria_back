const express = require("express");
const { consultRoles } = require("../controllers/role-controller");
const router = express.Router();

router.get("/consult", consultRoles);

module.exports = router;
