const express = require("express");
const { validatorLogin } = require("../middleware/login-validator");
const { login } = require("../controllers/login-controller");
const router = express.Router();

router.post("/", validatorLogin, login);

module.exports = router;