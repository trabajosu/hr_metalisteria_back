# Hr_Metalisteria_Back 
A continuación se anexa el video de instalación del proyecto 
* (Link de instalación https://youtu.be/wfCSV5hrvQM)
* Link de demostración de la App (https://youtu.be/W-fJmEd7Nm8)
## Project for the management of the company HR Taller Y Metalistería

# Resumen de pasos

* 1. Ingresar a Gitlab para proceder a descargar el back end y el front end 
* 2. Se debe ingresar al apartado TrabajosU para encontrar los repositorios 
* 3. Luego empezamos con la descarga del primero repositorio en este caso el back end
* 4. Mientras descarga, haremos la instalación de las herramientas necesarias para el correcto funcionamiento de app
    * Descargar la ultima versión de Node estable 16.14.12 lts
    * Descargar un editor de codigo, en este visual estudio code
    * Descargar el motor de base de datos MYSQL workbench 
* 5. Ahora si pasamos a descargar el repositorio del backend
* 6. Descomprimimos el archivo y lo abrimos en el editor
* 7. En la consola del editor copiamos el comando npm i para instalar los paquetes del proyecto
* 8. Luego buscamos en las carpetas del backend  la carpeta db y copiamos lo que esta en el archivo data.sql, alli se encuentra script de la base de datos con la que trabajamos 
* 9. vamos a el workbench y pegamos este escript y asi mismo lo ejecutamos para crear la bd 
* 10. Luego creamos el archivo .env en la raiz del proyecto y creamos las variables de entorno 
* 11. seguidamente escribimos el comando npm run dev para ejecutar el backend 
* 12. para provar el funcionamiento copiamos la siguiente URL localhost:10101/city/consult en el navegador   
* 13. para instalar el front
* 14. instalamos las herramientas o paquetes como: 
    * Typescript con el comando install -g typescript 
    * verificamos la correcta instalacion con el comando tsc --version
    * instalar el gestor de angular, con npm install -g @angular/cli
* 15. Luego descagarmos el repositorio del front end
* 16. Descomprimos el archivo 
* 17. Ejecutamos los siguientes comando para poner en funcionamiento esta parte
    * escribimos el comando npm i para instalar todos los paquetes 
    * luego iniciamos el servidor con npm run start 
18. y por ultimo miramos el funcionamiento con la URL localhosto:4200




