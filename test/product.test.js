const { server } = require("../index");
const { api, getToken, createItem, deleteItem } = require("./helpers");

describe('Registering a product in the application', () => {

    test("a valid product can be register", async () => {
        const newProduct = {
            code: "12345",
            name: "Estufa",
            description: "estufa en acero inoxidable",
            iva: "19",
            price: "200000",
        };

        const token = await getToken();

        await api
            .post("/product/register")
            .send(newProduct)
            .set({ Authorization: `Bearer ${token}` })
            .expect(200)
            .expect("Content-Type", /application\/json/);

        const code = newProduct.code;
        await deleteItem(`/product/delete/?code=${code}`);
    });

    test("product without name cannot be registered", async () => {
        const newProduct = {
            code: "12345",
            description: "estufa en acero inoxidable",
            iva: "19",
            price: "200000",
        };
        const token = await getToken();
        await api
            .post("/product/register")
            .send(newProduct)
            .set({ Authorization: `Bearer ${token}` })
            .expect(400)
            .expect("Content-Type", /application\/json/);
    });

    test("a valid product can be updated", async () => {
        const token = await getToken();

        const newProduct = {
            code: "002",
            name: "Estufa",
            description: "estufa en acero inoxidable",
            iva: "19",
            price: "200000",
        };
        await createItem("/product/register", newProduct);

        const newProductUpdate = {
            code: "002",
            name: "Estufa",
            description: "estufa en acero inoxidable con nuevas caracteristicas",
            iva: "19",
            price: "200000",
        };
        const code = newProduct.code;

        await api
            .put(`/product/update/?code=${code}`)
            .send(newProductUpdate)
            .set({ Authorization: `Bearer ${token}` })
            .expect(200)
            .expect("Content-Type", /application\/json/);

        await deleteItem(`/product/delete/?code=${code}`);
    });

    test("product created for delete", async () => {
        const newProductDelete = {
            code: "003",
            name: "Estufa",
            description: "estufa en acero inoxidable",
            iva: "19",
            price: "200000",
        };
    
        await createItem("/product/register", newProductDelete);
    
        const token = await getToken();
        const code = newProductDelete.code;
    
        await api
          .delete(`/product/delete/?code=${code}`)
          .set({ Authorization: `Bearer ${token}` })
          .expect(200)
          .expect("Content-Type", /application\/json/);
      });
});
afterAll(() => {
    server.close();
});