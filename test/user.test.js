
const { api, createItem, getToken, deleteItem } = require("./helpers");

beforeEach(async () => {

});

//* REGISTER USER
describe('Registering a user in the application', () => {
  test("user are returned as json", async () => {
    await api
      .get("/user/consultAll")
      .expect(403)
      .expect("Content-Type", /application\/json/);
  });

  test("a valid user can be registered", async () => {
    const newUser = {
      name: "user register test",
      email: "userregistertest@gmail.com",
      password: "12345",
      address: "address test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };

    const result = await api
      .post("/user/register")
      .send(newUser)
      .expect(200)
      .expect("Content-Type", /application\/json/);

    const id = JSON.parse(result.text).user.insertId;
    await deleteItem(`/user/adminDelete/?id=${id}`);
  });

  test("user without name cannot be registered", async () => {
    const newUser = {
      email: "userwithoutname@gmail.com",
      password: "12345",
      address: "address test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };

    await api
      .post("/user/register")
      .send(newUser)
      .expect(400)
      .expect("Content-Type", /application\/json/);
  });

  test("user with existing email cannot be registered", async () => {
    const newUser = {
      name: "user register test",
      email: "test@gmail.com",
      password: "12345",
      address: "address test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };

    const result = await createItem("/user/register", newUser);
    await api
      .post("/user/register")
      .send(newUser)
      .expect(400)
      .expect("Content-Type", /application\/json/);

    const id = JSON.parse(result.text).user.insertId;
    await deleteItem(`/user/adminDelete/?id=${id}`);
  });

  test("a valid user can be registered by an administrator", async () => {

    const token = await getToken();

    const newUser = {
      name: "user register admin test",
      email: "userregisteradmintest@gmail.com",
      password: "12345",
      address: "address admin test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };

    const result = await api
      .post("/user/adminRegister")
      .send(newUser)
      .set({ Authorization: `Bearer ${token}` })
      .expect(200)
      .expect("Content-Type", /application\/json/);

    const id = JSON.parse(result.text).user.insertId;
    await deleteItem(`/user/adminDelete/?id=${id}`);
  });
});

//* UPDATE USER
describe('Updating a user in the application', () => {
  test("a valid user can be updated", async () => {
    const newUser = {
      name: "user register test",
      email: "userregistertest@gmail.com",
      password: "12345",
      address: "address test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };
    const result = await createItem("/user/register", newUser);
    const id = JSON.parse(result.text).user.insertId;

    const token = await getToken();
    const newUserInfo = {
      name: "user update test",
      email: "userupdatetest@gmail.com",
      address: "address update test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
      roleCode: 2
    };

    await api
      .put(`/user/adminUpdate/?id=${id}`)
      .send(newUserInfo)
      .set({ Authorization: `Bearer ${token}` })
      .expect(200)
      .expect("Content-Type", /application\/json/);

    await deleteItem(`/user/adminDelete/?id=${id}`);
  });

  test("user without name cannot be updated", async () => {
    const newUser = {
      email: "userupdatewithoutname@gmail.com",
      password: "12345",
      address: "address update test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };
    const token = await getToken();

    await api
      .put("/user/adminUpdate/?id=0")
      .send(newUser)
      .set({ Authorization: `Bearer ${token}` })
      .expect(400)
      .expect("Content-Type", /application\/json/);
  });
});

//* DELETE USER
describe('Deleting a user in the application', () => {
  test("a user with a valid id can be deleted", async () => {
    const newUser = {
      name: "user delete test",
      email: "userdeletetest@gmail.com",
      password: "12345",
      address: "address delete test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };
    const result = await createItem("/user/register", newUser);
    const id = JSON.parse(result.text).user.insertId;
    const token = await getToken();

    await api
      .delete(`/user/adminDelete/?id=${id}`)
      .set({ Authorization: `Bearer ${token}` })
      .expect(200)
      .expect("Content-Type", /application\/json/);

    await deleteItem(`/user/adminDelete/?id=${id}`);
  });

  test("a user with invalid id cannot be deleted", async () => {
    const token = await getToken();

   result = await api
      .delete(`/user/adminDelete/?id=test`)
      .set({ Authorization: `Bearer ${token}` })
      .expect(400)
      .expect("Content-Type", /application\/json/);
  });
});

// afterAll(() => {
//   server.close();
// });
