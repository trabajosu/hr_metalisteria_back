const { server } = require("../index");
const { api, createItem, getToken, deleteItem } = require("./helpers");

beforeEach(async () => {

});

describe('Registering a local in the application', () => {

  test("local are returned as json", async () => {
    await api
      .get("/local/consult")
      .expect(200)
      .expect("Content-Type", /application\/json/);
  });

  test("a valid local can be registered", async () => {

      const token = await getToken();

      const newlocal = {
          name: 'Local1',
          address: 'La patria',
          cityCode: 1
      };

      const result = await api
      .post("/local/register")
      .send(newlocal)
      .set({ Authorization: `Bearer ${token}` })
      .expect(200)
      .expect("Content-Type", /application\/json/);

      const id = JSON.parse(result.text).locals.insertId;
      await deleteItem(`/local/delete/?id=${id}`);
  });

  test("Local without name cannot be registered", async () => {
      const token = await getToken();

      const newlocal = {
          address: 'La patria',
          cityCode: 1
      };

      await api
      .post("/local/register")
      .send(newlocal)
      .expect(403)
      .expect("Content-Type", /application\/json/);
  });

  test("update Local", async () => {

      const token = await getToken();
      const newlocal = {
          name: 'Local1',
          address: 'La patria',
          cityCode: 1
      };
      const result = await createItem("/local/register", newlocal);

      const updateLocal = {
          id: result.insertId,
          name: 'Local12',
          address: 'La patria',
          cityCode: 1
      };

      let idLocal = JSON.parse(result.text).locals.insertId;

      await api
          .put(`/local/update?id=${idLocal}`)
          .send(updateLocal)
          .set({ Authorization: `Bearer ${token}` })
          .expect(200)
          .expect("Content-Type", /application\/json/);

      await deleteItem(`/local/delete/?id=${idLocal}`);
  });

  test("update Local Missing Id ", async () => {

    const token = await getToken();

    const updateLocal = {
        id: 1,
        name: 'Local12',
        address: 'La patria',
        cityCode: 1
    };

    await api
        .put(`/local/update?id=1`)
        .send(updateLocal)
        .set({ Authorization: `Bearer ${token}` })
        .expect(400)
        .expect("Content-Type", /application\/json/);
  });

    // await deleteItem(JSON.parse(result.text).user.insertId);
  test("Delete Local error  Missing Id ", async () => {

      const token = await getToken();

      await api
          .delete(`/local/delete?id=00`)
          .set({ Authorization: `Bearer ${token}` })
          .expect(400)
          .expect("Content-Type", /application\/json/);
  });

  test("Delete Local Bad Url  ", async () => {
    const token = await getToken();

    await api
        .delete(`/local/delete`)
        .set({ Authorization: `Bearer ${token}` })
        .expect(400)
        .expect("Content-Type", /application\/json/);
  });

});
afterAll(() => {
  server.close();
});
