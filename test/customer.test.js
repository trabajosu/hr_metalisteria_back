const { server } = require("../index");
const { api, getToken, createItem, deleteItem} = require("./helpers");

describe('Registering a customer in the application', () => {

  test("a valid customer can be register", async () => {
    const newUser = {
      document: "1094959423",
      name: "Test customer",
      email: "customerregistertest@gmail.com",
      phoneNumber: "3118686867",
      address: "address customer test",
      cityCode: 1
    };
   
    const token = await getToken();

    await api
      .post("/customer/register")
      .send(newUser)
      .set({ Authorization: `Bearer ${token}` })
      .expect(200)
      .expect("Content-Type", /application\/json/);

    const document = newUser.document;
    await deleteItem(`/customer/delete/?document=${document}`);
  });

  test("user without name cannot be registered", async () => {
    const newUser = {
      document: "123456789",
      email: "customer@gamil.com",
      phoneNumber: "3118686867",
      address: "address customer test",
      cityCode: 1
    };
    const token = await getToken();
    await api
      .post("/customer/register")
      .send(newUser)
      .set({ Authorization: `Bearer ${token}` })
      .expect(400)
      .expect("Content-Type", /application\/json/);
  });

  test("a valid customer can be updated", async () => {
    const token = await getToken();

    const newCustomer = {
      document: "123456789",
      name: "Test customer",
      email: "customerregistertest@gmail.com",
      phoneNumber: "3118686867",
      address: "address customer test",
      cityCode: 1
    };
    await createItem("/customer/register", newCustomer);
      
      const newcustomerUpdate = {
        document: "123456789",
        name: "Test customer",
        email: "customerUpdate@gmail.com",
        phoneNumber: "3118686867",
        address: "address customer test",
        cityCode: 1
      };
      const document =  newCustomer.document;

    await api
      .put(`/customer/update/?document=${parseInt(document)}`)
      .send(newcustomerUpdate)
      .set({ Authorization: `Bearer ${token}` })
      .expect(200)
      .expect("Content-Type", /application\/json/);

      await deleteItem(`/customer/delete/?document=${document}`);
  });

  test("customer created", async () => {
    const newUserDelete = {
      document: "123489898",
      name: "Test customer delete",
      email: "customerdelete@gamil.com",
      phoneNumber: "3145653657",
      address: "address customer test",
      cityCode: 1
    };
    
    await createItem("/customer/register", newUserDelete);
    
    const token = await getToken();
    const document = newUserDelete.document;

    await api
      .delete(`/customer/delete/?document=${document}`)
      .set({ Authorization: `Bearer ${token}` })
      .expect(200)
      .expect("Content-Type", /application\/json/);
  });

})

afterAll(() => {
  server.close();
});