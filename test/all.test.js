const { api, createItem, getToken, deleteItem } = require("./helpers");

//* REGISTER USER
describe('Registering a user in the application', () => {
  test("user are returned as json", async () => {
    await api
      .get("/user/consultAll")
      .expect(403)
      .expect("Content-Type", /application\/json/);
  });

  test("user without name cannot be registered", async () => {
    const newUser = {
      email: "userwithoutname@gmail.com",
      password: "12345",
      address: "address test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };

    await api
      .post("/user/register")
      .send(newUser)
      .expect(400)
      .expect("Content-Type", /application\/json/);
  });
});

//* UPDATE USER
describe('Updating a user in the application', () => {

  test("user without name cannot be updated", async () => {
    const newUser = {
      email: "userupdatewithoutname@gmail.com",
      password: "12345",
      address: "address update test",
      phoneNumber: "1234567890",
      localCode: 1,
      cityCode: 1,
    };
    const token = await getToken();

    await api
      .put("/user/adminUpdate/?id=0")
      .send(newUser)
      .set({ Authorization: `Bearer ${token}` })
      .expect(400)
      .expect("Content-Type", /application\/json/);
  });
});

//* DELETE USER
describe('Deleting a user in the application', () => {

  test("a user with invalid id cannot be deleted", async () => {
    const token = await getToken();

   result = await api
      .delete(`/user/adminDelete/?id=test`)
      .set({ Authorization: `Bearer ${token}` })
      .expect(400)
      .expect("Content-Type", /application\/json/);
  });
});

describe('Registering a customer in the application', () => {

  test("user without name cannot be registered", async () => {
    const newUser = {
      document: "123456789",
      email: "customer@gamil.com",
      phoneNumber: "3118686867",
      address: "address customer test",
      cityCode: 1
    };
    const token = await getToken();
    await api
      .post("/customer/register")
      .send(newUser)
      .set({ Authorization: `Bearer ${token}` })
      .expect(400)
      .expect("Content-Type", /application\/json/);
  });

})

describe('Registering a local in the application', () => {

  test("Local without name cannot be registered", async () => {
      const token = await getToken();

      const newlocal = {
          address: 'La patria',
          cityCode: 1
      };

      await api
      .post("/local/register")
      .send(newlocal)
      .expect(403)
      .expect("Content-Type", /application\/json/);
  });

    // await deleteItem(JSON.parse(result.text).user.insertId);
  test("Delete Local error  Missing Id ", async () => {

      const token = await getToken();

      await api
          .delete(`/local/delete?id=00`)
          .set({ Authorization: `Bearer ${token}` })
          .expect(400)
          .expect("Content-Type", /application\/json/);
  });

  test("Delete Local Bad Url  ", async () => {
    const token = await getToken();

    await api
        .delete(`/local/delete`)
        .set({ Authorization: `Bearer ${token}` })
        .expect(400)
        .expect("Content-Type", /application\/json/);
  });
});

describe('Registering a product in the application', () => {

  test("product without name cannot be registered", async () => {
      const newProduct = {
          code: "12345",
          description: "estufa en acero inoxidable",
          iva: "19",
          price: "200000",
      };
      const token = await getToken();
      await api
          .post("/product/register")
          .send(newProduct)
          .set({ Authorization: `Bearer ${token}` })
          .expect(400)
          .expect("Content-Type", /application\/json/);
  });

});